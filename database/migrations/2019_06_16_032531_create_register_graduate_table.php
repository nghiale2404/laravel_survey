<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRegisterGraduateTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('register_graduate')) {
            Schema::create('register_graduate', function (Blueprint $table) {
                $table->increments('register_graduate_id')->comment('id mau dang ky tot nghiep');
                $table->string('register_graduate_semester', 50)->comment('học kỳ tốt nghiệp');
                $table->string('register_graduate_session', 50)->comment('năm tốt nghiệp');
                $table->date('register_graduate_date')->comment('ngày đăng ký tốt nghiệp');     // QUAN TRONG: nó quyết định luôn years_end. Vì vậy mới bỏ bảng years và year_users
                $table->double('register_graduate_GPA')->comment('điểm trung bình');
                $table->double('register_graduate_DRL')->comment('điểm rèn luyện');
                $table->integer('register_graduate_TCTL')->comment('tổng tín chỉ tích lũy');
                $table->string('register_graduate_ranked', 50)->comment('xếp loại');
                $table->string('register_graduate_degree', 50)->comment('danh hiệu ');

                //log time
                $table->timestamp('created_at')
                    ->default(DB::raw('CURRENT_TIMESTAMP'))
                    ->comment('ngày tạo');

                $table->timestamp('updated_at')
                    ->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'))
                    ->comment('ngày cập nhật');

                $table->timestamp('deleted_at')
                    ->nullable()
                    ->comment('ngày xóa tạm');
            });
            DB::statement("ALTER TABLE `register_graduate` comment 'Mẫu đăng ký tốt nghiệp '");
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }
}
