<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWorksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('works')) {
            Schema::create('works', function (Blueprint $table) {
                $table->increments('work_id')->comment('id công việc');
                $table->integer('company_id')->unsigned()->comment('id công ty đang làm việc');
                $table->string('work_name')->comment('tên công việc');
                $table->string('work_position')->comment('chức vụ trong công việc ');
                $table->text('work_note')->comment('ghi chú');

                //log time
                $table->timestamp('created_at')
                    ->default(DB::raw('CURRENT_TIMESTAMP'))
                    ->comment('ngày tạo');

                $table->timestamp('updated_at')
                    ->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'))
                    ->comment('ngày cập nhật');

                $table->timestamp('deleted_at')
                    ->nullable()
                    ->comment('ngày xóa tạm');
            });
            DB::statement("ALTER TABLE `works` comment 'Thông tin công việc mà sinh viên đang làm'");
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }
}
