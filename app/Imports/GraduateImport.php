<?php

namespace App\Imports;

use App\Models\RegisterGraduate;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class GraduateImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        $register_graduate = new RegisterGraduate();
        // dd($alumni);
        $register_graduate->register_graduate_semester = $row[0];
        $register_graduate->register_graduate_session = $row[1];
        $register_graduate->register_graduate_date = \PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($row[2]);
        $register_graduate->register_graduate_decide = $row[3];
        $register_graduate->register_graduate_GPA = $row[4];
        $register_graduate->register_graduate_DRL = $row[5];
        $register_graduate->register_graduate_TCTL = $row[6];
        $register_graduate->register_graduate_ranked = $row[7];
        $register_graduate->register_graduate_degree = $row[8];
        return $register_graduate;
    }
}
