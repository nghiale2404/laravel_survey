<?php

namespace App\Models;

use App\Models\Base\BaseModel;

class RegisterGraduate extends BaseModel
{
    protected $table = 'register_graduate';

    protected $primaryKey = 'register_graduate_id';

    protected $keyType = 'int';

    protected $fillable = [
        'register_graduate_id',
        'register_graduate_semester',
        'register_graduate_session',
        'register_graduate_date',       
        'register_graduate_decide',
        'register_graduate_GPA',
        'register_graduate_DRL',
        'register_graduate_TCTL',
        'register_graduate_ranked',
        'register_graduate_degree',
        'created_at',
        'updated_at',
        'deleted_at',
    ];
    public $timestamps = true;
}
