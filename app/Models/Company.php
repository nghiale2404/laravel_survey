<?php

namespace App\Models;

use App\Models\Base\BaseModel;

class Company extends BaseModel
{
    protected $table = 'companies';

    protected $primaryKey = 'company_id';

    protected $keyType = 'int';

    protected $fillable = [
        'company_id',
        'company_name',
        'company_address',
        'company_title',
        'company_email',
        'company_tel',
        'company_website',
        'created_at',
        'updated_at',
        'deleted_at',
    ];
    public $timestamps = true;
}
