<?php

namespace App\Models;

use Illuminate\Http\Request;
use App\Models\Base\BaseModel;

class Classes extends BaseModel
{
    protected $table = 'classes';

    protected $primaryKey = 'class_id';

    protected $keyType = 'int';

    protected $fillable = [
        'class_id',
        'major_branch_id',
        'class_code',
        'class_name',
        'class_begin',
        'class_end',
        'created_at',
        'updated_at',
        'deleted_at',
    ];
    public $timestamps = true;

    public function __construct()
    {
        $this->fillable_list = $this->fillable;         // trường fillable sẽ truyền vào biến fillable_list
    }

    public function base_update(Request $request)
    {
        // $filter_param = $request->only($this->$fillable);
        $this->update_conditions = [
            'class_id' => 1,
        ];

        return parent::base_update($this->request);
    }
}
