<?php

namespace App\Models;

use App\Models\Base\BaseModel;
use Illuminate\Http\Request;
use App\Models\Question;
use App\Models\Survey;
use App\Models\Answer;

class SurveyQuestion extends BaseModel
{
    protected $table = 'survey_questions';

    protected $primaryKey = 'survey_question_id';

    protected $keyType = 'int';

    protected $fillable = [
        'survey_question_id',
        'survey_id',
        'question_id',
        'created_at',
        'updated_at',
        'deleted_at',
    ];
    public $timestamps = true;

    public function __construct()
    {
        $this->fillable_list = $this->fillable;
    }

    public function base_update(request $request)
    {
        $this->update_conditions = [
            'survey_question_id' => 1,
        ];

        return parent::base_update($this->request);
    }

    public function answers()
    {
        return $this->hasMany(Answer::class, 'survey_question_id', 'survey_question_id');
    }
    public function questions()
    {
        return $this->hasMany(Question::class, 'question_id', 'question_id');
    }
    public function surveys()
    {
        return $this->hasMany(Survey::class, 'survey_id', 'survey_id');
    }
}
