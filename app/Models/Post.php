<?php

namespace App\Models;

use App\Models\Base\BaseModel;

class Post extends BaseModel
{
    protected $table = 'posts';

    protected $primaryKey = 'post_id';

    protected $keyType = 'int';

    protected $fillable = [
        'post_id',
        'user_id',
        'role_id',
        'class_id',
        'post_title',
        'post_content',
        'post_slug',
        'post_status',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public $timestamps = true;
}
