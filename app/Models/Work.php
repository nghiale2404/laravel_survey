<?php

namespace App\Models;


use App\Models\Base\BaseModel;

class Work extends BaseModel

{
    protected $table = 'works';

    protected $primaryKey = 'work_id';

    protected $keyType = 'int';

    protected $fillable = [
        'work_id',
        'work_name',
        'work_position',
        'work_note',
        'company_id',
        'created_at',
        'updated_at',
        'deleted_at',
    ];
    public $timestamps = true;
}
