<?php
	
namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Survey;
use App\Models\Question;
use App\Models\SurveyQuestion;
use DB;

Class QuestionController extends Controller{

	// Ham khoi tao
	public function __construct(){
		parent::__construct();
	}

	public function store(Request $request,Survey $survey) 
    { 		
		$dt='';
		if($request->question_option){
		foreach($request->question_option as $key => $value){
			//$dt=$dt+$value;
			$dt= $dt.'/option'.$value;
		}}
		//return $dt[1];
		//return $request->question_option;
		$question_id=Question::insertGetId([
			'question_title'=>$request->question_title,
			'question_type'=>$request->question_type,
			'question_option'=>$dt,
			
		]);
		SurveyQuestion::insert([
			'survey_id' => $survey->survey_id,
			'question_id' => $question_id,

		//$insert_ques=DB::table('questions')->insert($option);
		]);
		return back();
	}

    public function edit(Question $question) 
    {
      return view('question.edit', compact('question'));
    }

    public function update(Request $request, Question $question) 
    {

      $question->update($request->all());
      return redirect()->action('SurveyController@detail_survey', [$question->survey_id]);
    }


	// public function index(request $request){
	// 	$config = [
	// 		'model' => new Question(),
	// 		'request' => $request
	// 	];
	// 	$this->config($config);
	// 	$data = $this->model->web_index($this->request);
	// 	return view('pages.admins.question.index',['data'=>$data]);
	// }

	// public function create_render(request $request){
	// 	return view('pages.admins.question.create');
	// }

	// public function create_submit(request $request){
	// 	$config = [
	// 		'model' => new Question(),
	// 		'request' => $request
	// 	];
	// 	$this->config($config);
	// 	$data = $this->model->web_insert($this->request);
	// 	return redirect('questions')->with('success','Added Successfully');
	// }


	// public function edit($question_id){
	// 	$question = Question::findOrFail($question_id);
	// 	return view('pages.admins.question.edit',compact('question','question_id'));
	// }

	// public function update(request $request,$question_id){
	// 	$question =	 Question::find($question_id);

	// 	$question->question_content = $request->get('question_content');
	// 	$question->question_type	= $request->get('question_type');
	// 	$question->question_answer	= $request->get('question_answer');
  		
  	// 	$question->save();

  	// 	return redirect('questions')->with('success','Succsssfully');
	// }

	// public function destroy($question_id){
	// 	$data = Question::findOrFail($question_id);
	// 	$data->delete();
	// 	return redirect('questions')->with('success','Successfully');	
	// }

}