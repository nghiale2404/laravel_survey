<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Survey;
use App\Models\Question;
use App\Models\Answer;
use App\Models\User;
use App\Models\SurveyQuestion;

class SurveyController extends Controller{
	//khoi tao
	public function __construct(){
			parent::__construct();
	}
	public function home(Request $request) 
	  {
	    $surveys = Survey::get();
	    return view('home', compact('surveys'));
	  }
	public function index(Request $request)
	    {
	    $config = [
	      'model' => new Survey(),
	      'request' => $request,
	    ];
	    $this->config($config);
	    $survey = $this->model->web_index($this->request);

	    return view('pages.admins.survey.index', ['survey' => $survey]);
	    }
  # Show page to create new survey
  public function create_render() 
  {
    return view('pages.admins.survey.create');
  }

  public function create_submit(Request $request) 
  {
  	//    $arr = $request->all();
    // // $request->all()['user_id'] = Auth::id();
    // $arr['user_id'] = Auth::id();
    // $surveyItem = $survey->create($arr);
    // return Redirect::to("/survey/{$surveyItem->id}");
    $config = [
            'model' => new Survey(),
            'request' => $request,
        ];
        $this->config($config);
        $data = $this->model->web_insert($this->request);
    return redirect("/survey/{$this->request->survey_id}");
  }

  # retrieve detail page and add questions here
  public function detail_survey(Survey $survey) 
  {
    //return $survey;
    $surveyquestion=SurveyQuestion::select('question_id')->where('survey_id',$survey->survey_id)->get();
    //  dd($surveyquestion);
    // return $surveyquestion;
    //$questions=Question::where('question_id',$surveyquestion)->get();
    // return $questions;
    // dd($questions);
    $survey->load('surveyquestions');
    // return $survey;
    return view('pages.admins.survey.detail', compact('survey'));
  }
  

  public function edit(Survey $survey) 
  {
    $survey_id = Survey::findOrFail($survey->survey_id);
    
    return view('pages.admins.survey.edit',compact('survey','survey_id'));
  }

  # edit survey
  // public function update(Request $request, Survey $survey) 
  // {
  //   $survey->update($request->only(['title', 'description']));
  //   return redirect()->action('SurveyController@detail_survey', [$survey->survey_id]);
  // }

  # view survey publicly and complete survey
  public function view_survey(Survey $survey) 
  { 
    // $survey->option_name = unserialize($survey->option_name);
    // $survey->survey_option = unserialize($survey->survey_option);
    // dd($survey);
    // $survey->load('surveyquestions');
    // $survey_question_id=SurveyQuestion::
    return view('pages.admins.survey.view', compact('survey'));
  }

  # view submitted answers from current logged in user
  public function view_survey_answers(Survey $survey) 
  {
    $survey->load('users.questions.answers');
    // return view('survey.detail', compact('survey'));
    // return $survey;
    return view('pages.admins.answer.view', compact('survey'));
  }

  // TODO: Make sure user deleting survey
  // has authority to



	public function update(request $request, $survey_id){
		$survey = Survey::find($survey_id);
//
		$survey->user_id			=	'1';
		$survey->survey_name		=	$request->get('survey_name');
		$survey->survey_description	=	$request->get('survey_description');
		$survey->survey_start		=	$request->get('survey_start');
		$survey->survey_finish		=	$request->get('survey_finish');
		$survey->survey_version		=	$request->get('survey_version');

		$survey->save();

		return redirect('survey')->with('success','Updated Successfully!');
	}

	public function destroy ($survey_id){
		$data = Survey::findOrFail($survey_id);
		$data->delete();

		return redirect('survey')->with('success','Deleted Successfully!');
	}
}