<head>
    @include('layouts.admin.meta')
    @include('layouts.admin.title')
    @include('layouts.admin.css')
    @include('layouts.admin.custom_css')
</head>
