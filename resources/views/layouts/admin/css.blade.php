<link rel="icon" type="image/png" sizes="16x16" href="{{asset('/public/images/users/favicon.png')}}">
<!-- Bootstrap Core CSS -->
<link href="{{asset('/public/bootstrap/dist/css/bootstrap.min.css')}}" rel="stylesheet">
<link href="{{asset('/public/plugins/bower_components/bootstrap-extension/css/bootstrap-extension.css')}}" rel="stylesheet">
<!-- Menu CSS -->
<link href="{{asset('/public/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css')}}" rel="stylesheet">

<!-- animation CSS -->
<link href="{{asset('/public/css/animate.css')}}" rel="stylesheet">
<!-- Custom CSS -->
<link href="{{asset('/public/css/style.css')}}" rel="stylesheet">
<!-- color CSS -->
<link href="{{asset('/public/css/style.css')}}" rel="stylesheet">
<link href="{{asset('/public/css/colors/blue.css')}}" id="theme" rel="stylesheet">
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
