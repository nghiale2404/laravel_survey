@extends('layouts.admin')
@section('content')
<!-- Page Content -->
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
                @if($message = Session::get('success'))
                <div class="alert alert-success" role="alert">
                    <p>{{$message}}</p>
                    <p class="mb-0"></p>
                </div>
                @endif
            <div class="col-sm-12">
                <div class="white-box">
                    <h3 class="box-title m-b-0">Data Table</h3>
                    <p class="text-muted m-b-30">Data table example</p>
                    <div class="table-responsive">
                        <div id="myTable_wrapper" class="dataTables_wrapper no-footer">
                            <div class="dataTables_length" id="myTable_length">
                                <label>Show 
                                    <select name="myTable_length" aria-controls="myTable" class="">
                                        <option value="10">10</option>
                                        <option value="25">25</option>
                                        <option value="50">50</option>
                                        <option value="100">100</option>
                                    </select> entries
                                </label>
                                </div>
                                <div id="myTable_filter" class="dataTables_filter">
                                    <label>Search:
                                        <input type="search" class="" placeholder="" aria-controls="myTable">
                                    </label>
                                </div>
                                <table id="myTable" class="table table-striped dataTable no-footer" role="grid" aria-describedby="myTable_info">
                                    <br>
                                    <a href="{{route('class/create')}}" class="btn btn-success">Add</a>
                                    <br>
                                    <thead>
                                        {{-- <tr role="row"><th class="sorting_asc" tabindex="0" aria-controls="myTable" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Code: activate to sort column descending" style="width: 104px;"></th> --}}
                                            <th class="sorting" tabindex="0" aria-controls="myTable" rowspan="1" colspan="1" aria-label="Class ID: activate to sort column ascending" style="width: 202px">Class ID:</th>
                                            <th class="sorting" tabindex="0" aria-controls="myTable" rowspan="1" colspan="1" aria-label="Class Code: activate to sort column ascending" style="width: 202px;">Class Code</th>
                                            <th class="sorting" tabindex="0" aria-controls="myTable" rowspan="1" colspan="1" aria-label="Class Name: activate to sort column ascending" style="width: 205px;">Class Name</th>
                                            <th class="sorting" tabindex="0" aria-controls="myTable" rowspan="1" colspan="1" aria-label="School Year Begin: activate to sort column ascending" style="width: 414px">School Year Begin</th>
                                            <th class="sorting" tabindex="0" aria-controls="myTable" rowspan="1" colspan="1" aria-label="School Year End: activate to sort column ascending" style="width: 414px">School Year End</th>
                                            <th class="sorting" tabindex="0" aria-controls="myTable" rowspan="1" colspan="1" aria-label="Major Branch: activate to sort column ascending" style="width: 414px;">Major Branch</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($data as $item)
                                            <tr>
                                                <td>{{$item['class_id']}}</td>
                                                <td>{{$item['class_code']}}</td>
                                                <td>{{$item['class_name']}}</td>
                                                <td>{{$item['class_begin']}}</td>
                                                <td>{{$item['class_end']}}</td>
                                                    @foreach($major_branch_name as $name)
                                                        @if ( $name->major_branch_id == $item['major_branch_id'] )
                                                            <td>{{$name->major_branch_name}}</td>
                                                        @endif
                                                    @endforeach
                                                <td>
                                                <form action="{{ route('class/destroy', $item->class_id) }}" method="post" class="delete_form">
                                                    @csrf
                                                    <a href="{{ route('class/show', $item->class_id) }}" class="btn btn-primary">Show</a>
                                                    <a href="{{ route('class/edit', $item->class_id) }}" class="btn btn-warning">Edit</a>
                                                    <button type="submit" class="btn btn-danger">Delete</button>
                                                </form>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                            </table>
                        <div class="dataTables_info" id="myTable_info" role="status" aria-live="polite">
                            Showing 1 to 10 of 57 entries
                        </div>
                        <div class="dataTables_paginate paging_simple_numbers" id="myTable_paginate">
                            <a class="paginate_button previous disabled" aria-controls="myTable" data-dt-idx="0" tabindex="0" id="myTable_previous">Previous</a><span><a class="paginate_button current" aria-controls="myTable" data-dt-idx="1" tabindex="0">1</a>
                                <a class="paginate_button " aria-controls="myTable" data-dt-idx="2" tabindex="0">2</a>
                                <a class="paginate_button " aria-controls="myTable" data-dt-idx="3" tabindex="0">3</a>
                                <a class="paginate_button " aria-controls="myTable" data-dt-idx="4" tabindex="0">4</a>
                                <a class="paginate_button " aria-controls="myTable" data-dt-idx="5" tabindex="0">5</a>
                                <a class="paginate_button " aria-controls="myTable" data-dt-idx="6" tabindex="0">6</a>
                            </span>
                            <a class="paginate_button next" aria-controls="myTable" data-dt-idx="7" tabindex="0" id="myTable_next">Next</a>
                        </div>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $('.delete_form').on('submit',function(){
            if(confirm('Are you sure delete id??'))
            {
                return true;
            }
            else
            {
                return false;
            }
        });
    });
</script>
<script>
    $(document).ready(function() {
        $('#myTable').DataTable();
        $(document).ready(function() {
            var table = $('#example').DataTable({
                "columnDefs": [{
                    "visible": false,
                    "targets": 2
                }],
                "order": [
                    [2, 'asc']
                ],
                "displayLength": 25,
                "drawCallback": function(settings) {
                    var api = this.api();
                    var rows = api.rows({
                        page: 'current'
                    }).nodes();
                    var last = null;

                    api.column(2, {
                        page: 'current'
                    }).data().each(function(group, i) {
                        if (last !== group) {
                            $(rows).eq(i).before(
                                '<tr class="group"><td colspan="5">' + group + '</td></tr>'
                            );

                            last = group;
                        }
                    });
                }
            });

            // Order by the grouping
            $('#example tbody').on('click', 'tr.group', function() {
                var currentOrder = table.order()[0];
                if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
                    table.order([2, 'desc']).draw();
                } else {
                    table.order([2, 'asc']).draw();
                }
            });
        });
    });
    $('#example23').DataTable({
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    });
</script>

@endsection