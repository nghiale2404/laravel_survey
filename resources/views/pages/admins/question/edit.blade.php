@extends('layouts.admin')
@section('content')
<form method="POST" action="/survey/question/{{ $question->question_id }}/update">
  <!-- {{ method_field('PATCH') }} -->
  <input type="hidden" name="_token" value="{{ csrf_token() }}">
  <h2 class="flow-text">Edit Question Title</h2>
   <div class="row">
    <div class="input-field col s12">
      <input type="text" name="title" id="title" value="{{ $question->question_title }}">
      <label for="title">Question</label>
    </div>
    <div class="input-field col s12">
    <button class="btn waves-effect waves-light">Update</button>
    </div>
  </div>
</form>
@endsection