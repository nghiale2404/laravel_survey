@extends('layouts.admin')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-6">
                @if (count($errors) > 0)
                    <div class="alert alert-danger" role="alert">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{  $error}}    </li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                @if (\Session::has('success'))
                    <div class="alert alert-success" role="alert">
                        <p>{{  \Session::get('success') }}</p>
                    </div>
                @endif
                <div class="white-box">
                    <h2 align="center">Thông tin về cựu sinh viên</h2>
                    <br>
                    <form data-toggle="validator" novalidate="true" action="{{route('alumnies.store')}}" method="POST">
                        @csrf
                        <div class="form-group">
                            <label for="code" class="control-label">Code</label>
                            <input type="text" class="form-control" id="code" name="code" placeholder="Code">
                        </div>
                        <div class="form-group">
                            <label for="first_name" class="control-label">First Name</label>
                            <input type="text" class="form-control" id="first_name" name="first_name" placeholder="First Name">
                        </div>
                        <div class="form-group">
                            <label for="last_name" class="control-label">Last Name</label>
                            <input type="text" class="form-control" id="last_name" name="last_name" placeholder="Last Name">
                        </div>
                        <div class="form-group">
                            <label for="username" class="control-label">UserName</label>
                            <input type="text" class="form-control" id="username" name="username" placeholder="UserName">
                        </div>
                        <div class="form-group">
                            <label for="password" class="control-label">Password</label>
                            <input type="text" class="form-control" id="password" name="password" placeholder="Password">
                        </div> 
                        <div class="form-group">
                            <label for="nation" class="control-label">Nation</label>
                            <input type="text" class="form-control" id="nation" name="nation" placeholder="Nation">
                        </div>  
                        <div class="form-group">
                            <label for="tel" class="control-label">Phone</label>
                            <input type="text" class="form-control" id="tel" name="tel" placeholder="Phone">
                        </div>
                        <div class="form-group">
                            <label for="email" class="control-label">Email</label>
                            <input type="email" class="form-control" id="email" name="email" placeholder="Email">
                        </div>
                        <div class="form-group">
                            <label for="gender" class="control-label">Gender</label>
                            <input type="text" class="form-control" id="gender" name="gender" placeholder="Gender">
                        </div>
                        <div class="form-group">
                            <label for="birthday" class="control-label">Birthday</label>
                            <input type="date" class="form-control" id="birthday" name="birthday" placeholder="Birthday">
                        </div>
                        <div class="form-group">
                            <label for="address" class="control-label">Address</label>
                            <input type="text" class="form-control" id="address" name="address" placeholder="Address">
                        </div>
                        <div class="form-group">
                            <label for="family_tel" class="control-label">Family Phone</label>
                            <input type="text" class="form-control" id="family_tel" name="family_tel" placeholder="Family Phone">
                        </div>
                        <div class="form-group">
                            <label for="family_address" class="control-label">Family Address</label>
                            <input type="text" class="form-control" id="family_address" name="family_address" placeholder="Family Address">
                        </div>
                        <div class="form-group">
                            <label for="status_id" class="control-label">Status</label>
                            <input type="text" class="form-control" id="status_id" name="status_id" placeholder="Status ID">
                            <small id="helpId" class="text-muted">Choose the Status ID: 2 Nghi hoc, 3 Di lam</small>
                        </div>
                        {{-- <div class="form-group">
                            <label for="status_users_reason" class="control-label">Reason</label>
                            <input type="text" class="form-control" id="status_users_reason" name="status_users_reason" placeholder="Reaon">
                        </div> --}}
                        <div class="form-group">
                            <button type="submit" class="btn btn-success">Submit</button>
                            <a href="{{route('alumnies.index')}}" class="btn btn-default">Back</a>
                        </div>
                    </form>
                </div>
                {{-- end white-box --}}
            </div>
            {{-- end col-sm-6 --}}
        </div>
    </div>
    
@endsection