@extends('layouts.admin')
@section('content')
<div class="row">
    <div class="col-sm-12">
        <div class="white-box">
            <br>
            <br>
            <a href="{{route('alumnies.create')}}" class="btn btn-success">Add</a>
            <br>
            <div class="div" align="right">
                    <form action="{{ route('alumnies.import') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <input type="file" name="file" accept=".xlsx">
                            <br>
                            <button type="submit" class="btn btn-success">Import</button>
                            <a class="btn btn-warning" href="{{ route('alumnies.export') }}">Export User Data</a>
                    </form>
                <form action="{{route('alumnies.import_register_graduate')}}" method="post" enctype="multipart/form-data">
                    @csrf
                    <input type="file" name="file">
                    <button type="submit" class="btn btn-danger">Import Graduate</button>
                </form> 
            </div>
            <br>
            <div class="table-responsive">
                <table id="table_pagination" class="table display">
                    <thead>
                        <tr>
                            <th>Code</th>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Username</th>
                            <th>Phone Number</th>
                            <th>Email</th>
                            <th>Gender</th>
                            <th>Birthday</th>
                            <th>Address</th>
                            <th>Status</th>
                            <th>Reason</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($users as $row)
                        <tr>
                            <td>{{$row['code']}}</td>
                            <td>{{$row['first_name']}}</td>
                            <td>{{$row['last_name']}}</td>
                            <td>{{$row['username']}}</td>
                            <td>{{$row['tel']}}</td>
                            <td>{{$row['email']}}</td>
                            <td>{{$row['gender']}}</td>
                            <td>{{$row['birthday']}}</td>
                            <td>{{$row['address']}}</td>
                            @foreach ($row->statuses as $status)
                                <td>{{$status->status_name}}</td>
                                <td>{{$status->status_reason}}</td>
                            @endforeach
                            <td>
                                <form action="{{route('alumnies.destroy',$row->user_id)}}" method="post" class="delete_form">
                                    <a href="{{route('alumnies.show',$row->user_id)}}" class="btn btn-primary">Show</a>
                                    <a href="{{route('alumnies.edit',$row->user_id)}}" class="btn btn-warning">Edit</a>
                                    @csrf
                                    <button type="submit" class="btn btn-danger">Delete</button>
                                </form>
                            </td>
                        </tr>
                            
                        @endforeach
                    </tbody>
                </table>
                {!! $users->links() !!}
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('.delete_form').on('submit',function(){
            if(confirm('Are you sure delete id??'))
            {
                return true;
            }
            else
            {
                return false;
            }
        });
    });
</script>
    
@endsection