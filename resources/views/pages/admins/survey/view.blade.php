@extends('layouts.admin')
@section('content')
  <div class="card">
      <div class="card-content">
      <span class="card-title"> Start taking Survey</span>
      <p>
        <span class="flow-text">{{ $survey->survey_title }}</span> <br/>
      </p>
      <p>  
        {{ $survey->survey_description }}
        <br/>Created by: <a href="">{{ $survey->users->last_name }} {{ $survey->users->first_name }}</a>
      </p>
      <div class="divider" style="margin:20px 0px;"></div>
      {{-- {!! Form::open(array('action'=>array('AnswerController@store', $survey->id))) !!} --}}
      <form method="POST" action="{{route('survey.complete',$survey)}}" id="boolean">
        @csrf
          @forelse ($survey->surveyquestions as  $question)
          {{-- {{dd($survey->sqids)}} --}}
          {{-- @foreach($q as $key=>$question) --}}
            <p class="flow-text">Question - {{ $question->question_title }}</p>
            {{-- {{dd($question)}} --}}
            {{-- {{dd($question->question_option)}} --}}
                @if($question->question_type === 'text')
                  <div class="input-field col s12">
                    <input id="answer" type="text" name="{{$question->question_id}}[answer]">
                    <label for="answer">Answer</label>
                  </div>

                @elseif($question->question_type === 'textarea')
                  <div class="input-field col s12">
                    <textarea id="textarea1" class="materialize-textarea" name="{{$question->question_id}}[answer]"></textarea>
                    <label for="textarea1">Textarea</label>
                  </div>

                @elseif($question->question_type === 'radio')
                <?php $str = $question->question_option;
                $arr=array_filter(explode('/option', $str)); ?>
                @foreach($arr as $key=>$value)
                    <p style="margin:0px; padding:0px;">
                      <input name="{{ $question->question_id }}[answer]" type="radio" id="{{ $key }}" />
                      <label for="{{ $key }}">{{ $value }}</label>
                    </p>
                    @endforeach

                @elseif($question->question_type === 'checkbox')
                <?php $str = $question->question_option;
                  $arr=array_filter(explode('/option', $str)) ;?>
                  @foreach($arr as $key=>$value)
                  <p style="margin:0px; padding:0px;">
                    <input type="checkbox" id="something{{ $key }}" name="{{ $survey->question_id }}[answer]" />
                    <label for="something{{$key}}">{{ $value }}</label>
                  </p>
                  @endforeach
                @endif 

              <div class="divider" style="margin:10px 10px;"></div>
          @empty
            <span class='flow-text center-align'>Nothing to show</span>
          @endforelse
          <div class="form-group">
              <button type="submit" class="btn btn-success">submit</button>
          </div>
@endsection