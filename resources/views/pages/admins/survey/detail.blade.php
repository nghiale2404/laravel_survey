@extends('layouts.admin')
@section('content')
  <div class="card">
      <div class="card-content p-20">
      {{-- <p>Survey title</p> --}}
      <p class="h1 p-0-0-10-0">{{ $survey->survey_name }}</p>
      {{-- <p>Survey discription</p> --}}
      <p class="h3">{{ $survey->survey_description }}</p>
      <br/>
      <a href='view/{{$survey->survey_id}}' class="btn btn-success">Take Survey</a> 
      <a href="{{$survey->survey_id}}/edit" class="btn btn-warning">Edit Survey</a>
      <a href="/survey/answers/{{$survey->survey_id}}" class="btn btn-primary">View Answers</a>

      <div class="divider" style="margin:20px 0px;"></div>
      <p class="flow-text center-align">Questions</p>
      <ul class="collapsible" data-collapsible="expandable">
          
          @forelse ($survey->surveyquestions as  $question)
          {{-- @foreach($q as $question) --}}
          <div>{{$question->question_title}}<span class="collapsible-header"><a href="/question/{{ $question->question_id }}/edit" style="margin-left:50px">Edit</a></span></div>

          {{-- <li style="box-shadow:none;"> --}}
            
            <div class="collapsible-body">
              <div style="margin:5px; padding:10px;">
                  {{-- <div>{{$question['question_type']}}</div> --}}
                  {{-- {!! Form::open() !!}
                    @if($question['question_type'] === 'text')
                      {{ Form::text('question_title')}}

                    @elseif($question['question_type'] === 'textarea')
                    <div class="row">
                      <div class="input-field col s12">
                        <textarea id="textarea1" class="materialize-textarea" rows="5"></textarea>
                        <label for="textarea1">Provide answer</label>
                      </div>
                    </div>

                    @elseif($question['question_type'] === 'radio')
                      @foreach($question->question_option as $key=>$value)
                        <p style="margin:0px; padding:0px;">
                          <input type="radio" id="{{ $key }}" />
                          <label for="{{ $key }}">{{ $value }}</label>
                        </p>
                      @endforeach

                    @elseif($question['question_type'] === 'checkbox')
                      @foreach($question->question_option as $key=>$value)
                      <p style="margin:0px; padding:0px;">
                        <input type="checkbox" id="{{ $key }}" />
                        <label for="{{$key}}">{{ $value }}</label>
                      </p>
                      @endforeach
                    @endif 
                  {!! Form::close() !!} --}}
              </div>
            </div>
          </li>
          {{-- @endforeach --}}
          @empty
            <span style="padding:10px;">Nothing to show. Add questions below.</span>
          
          @endforelse
      </ul>

      <h2 class="flow-text">Add Question</h2>

      <form method="POST" action="{{route('survey.store',$survey)}}" id="boolean">
        @csrf
        {{-- <input type="hidden" name="_token" value="{{ csrf_token() }}"> --}}
        
        {{-- <div class="row text-left"> --}}
          <div class="input-field col s12">
            <select class="browser-default" name="question_type" id="question_type">
              <option value="" disabled selected>Choose your option</option>
              <option value="text">Text</option>
              <option value="textarea">Textarea</option>
              <option value="checkbox">Checkbox</option>
              <option value="radio">Radio</option>
            </select>
          </div>                
          
          <div class="input-field col s12 ">
            <br>
            <label for="question_title">Question</label> <br>
            <input name="question_title" id="question_title" type="text" class="text-left">
            
          </div>  
          <!-- this part will be chewed by script in init.js -->
          <div class="form-g" id="form-g"></div>

          <div class="input-field col s12">
            <br>
          <button class="btn waves-effect btn btn-info text-left">Submit</button>
          </div>
        </div>
        </form>
    </div>
  </div>
  <script src="{{ URL::asset('public\jquery-3.4.1.min.js') }}"></script>
  <script >
      // for adding new option
  $(document).on('click', '.add-option', function() {
      $(".form-g").append(material);
    });
  
    // will replace .form-g class when referenced
    var material = '<div class="input-field col input-g s12"><br>' +
      '<input name="question_option[]" id="question_option[]" type="text" placeholder="Options">' +
      '<span style="margin-left:50px; color:red; cursor:pointer;"class="delete-option">Delete</span><br>' +
      // '<label for="question_option">Options</label><br>' +
      '<p class="add-option" style="cursor:pointer; color:green; margin-top:5px">Add Another</p>' +
      '</div>';
      $(document).on('click', '.delete-option', function() {
      $(this).parent(".input-field").remove();
      });
    
    
    // allow for more options if radio or checkbox is enabled
    $(document).on('change', '#question_type', function() {
      var selected_option = $('#question_type :selected').val();
      if (selected_option === "radio" || selected_option === "checkbox") {
        $(".form-g").html(material);
      } else {
        $(".input-g").remove();
      }
    });
  
  </script>
    
@stop