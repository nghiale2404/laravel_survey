@extends('layouts.admin')
@section('content')
<!-- Page Content -->
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
                @if($message = Session::get('success'))
                <div class="alert alert-success" role="alert">
                    <p>{{$message}}</p>
                    <p class="mb-0"></p>
                </div>
                @endif
            <div class="col-sm-12">
                <div class="white-box">
                    {{-- <h3 class="box-title m-b-0">Data Table</h3>
                    <p class="text-muted m-b-30">Data table example</p> --}}
                    <div class="table-responsive">
                        <div id="myTable_wrapper" class="dataTables_wrapper no-footer">
                                <table id="table_pagination" class="table table-striped dataTable no-footer" role="grid" aria-describedby="myTable_info">
                                    <br>
                                    <a href="{{route('students.create')}}" class="btn btn-success">Add</a>
                                    <br>
                                    <div class="card-body">
                                        <form action="{{ route('students.import') }}" method="POST" enctype="multipart/form-data">
                                            @csrf
                                            <input type="file" name="file" class="form-control">
                                            <br>
                                            <button class="btn btn-success">Import User Data</button>
                                            <a class="btn btn-warning" href="{{ route('students.export') }}">Export User Data</a>
                                        </form>
                                    </div>
                                    <thead>
                                        <tr role="row">
                                            <th>Code</th>
                                            <th>First Name</th>
                                            <th>Last Name</th>
                                            <th>Tel</th>
                                            <th>Email</th>
                                            <th>Class Code</th>
                                            <th>Class Name</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($students as $row)
                                        <tr>
                                            <td>{{$row['code']}}</td>
                                            <td>{{$row['first_name']}}</td>
                                            <td>{{$row['last_name']}}</td>
                                            <td>{{$row['tel']}}</td>
                                            <td>{{$row['email']}}</td>
                                            @foreach ($row->classes as $class)
                                                <td>{{$class->class_code}}</td>
                                                <td>{{$class->class_name}}</td>
                                            @endforeach
                                            <td>
                                                <form action="{{ route('students.destroy', $row->user_id) }}" method="post" class="delete_form">
                                                    @csrf
                                                    <a href="{{ route('students.show', $row->user_id) }}" class="btn btn-primary">Show</a>
                                                    <a href="{{ route('students.edit', $row->user_id) }}" class="btn btn-warning">Edit</a>
                                                    <button type="submit" class="btn btn-danger">Delete</button>
                                                </form>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $('.delete_form').on('submit',function(){
            if(confirm('Are you sure delete id??'))
            {
                return true;
            }
            else
            {
                return false;
            }
        });
    });
</script>
<script>
    $(document).ready(function() {
        $('#table_pagination').DataTable({
            processing: true,
            serverside: true,
            ajax: {
                url : "{{route('students.index')}}",
            },
            columns: [
                {
                    data: code,
                    name: code,
                },
                {
                    data: first_name,
                    name: first_name,
                },
                {
                    data: last_name,
                    name: last_name,
                },
                {
                    data: username,
                    name: username,
                },
                {
                    data: password,
                    name: password,
                },
                {
                    data: tel,
                    name: tel,
                },
                {
                    data: email,
                    name: email,
                },
                {
                    data: active_code,
                    name: active_code,
                },
                {
                    data: gender,
                    name: gender,
                },
                {
                    data: birthday,
                    name: birthday,
                },

            ]

        });
        
    });
</script>

@endsection